package com.example.databindingdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.databindingdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        val user: User = User()

        binding.user = user

        binding.setNameBtn.setOnClickListener {
            if(binding.firstNameField.text.toString() == "" || binding.lastNameField.text.toString() == ""){
                return@setOnClickListener
            }
            user.firstName = binding.firstNameField.text.toString()
            user.lastName = binding.lastNameField.text.toString()

            binding.firstNameField.text.clear()
            binding.lastNameField.text.clear()
        }

        binding.clearNameBtn.setOnClickListener {
            user.firstName = ""
            user.lastName = ""
        }
    }
}