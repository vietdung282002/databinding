package com.example.databindingdemo

import com.example.databindingdemo.BR
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class User: BaseObservable() {
    @get:Bindable
    var firstName: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.firstName)
        }

    @get:Bindable
    var lastName: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.lastName)
        }
}